
(function () {

	function gaDisable(propertyId) {
		window['ga-disable-' + propertyId] = true;
	}

	// check the cookie on startup. if the 'ga-disabled' cookie is set, disable GA tracking
	// for the stored property ID
	var match = document.cookie.match(/ga-disabled=(.*?)(?:$|;)/);
	if (match !== null) {
		gaDisable(match[1]);
	}

	// Opt-out function
	window.gaOptout = function () {
		if (ga !== undefined && ga.getByName !== undefined) {
			// Set to the same value as the web property used on the site
			var propertyId = ga.getByName('t0').get('trackingId');

			document.cookie = 'ga-disabled=' + propertyId + '; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
			gaDisable(propertyId);

			if (Drupal.settings.googleanalytics_opt_out.success_message !== '') {
				alert(Drupal.settings.googleanalytics_opt_out.success_message);
			}
		}
	};

	// Opt-in function (requires page reload)
	window.gaOptin = function () {
		document.cookie = 'ga-disabled=; expires=Thu, 01 Jan 1970 00:00:01 UTC; path=/';
	};

})();
