<?php

/**
 * Implements hook_variable_info
 */
function googleanalytics_opt_out_variable_info($options) {
	$variables['googleanalytics_opt_out_message'] = array(
		'type' => 'string',
		'title' => t('Opt-out Success Message', array(), $options),
		'default' => 'Opt-out successful',
		'description' => t('The alert (browser-) message that appears after an opt-out link has been clicked. Leave this empty to show no notification'),
		'required' => TRUE,
		'group' => 'googleanalytics',
		'localize' => TRUE,
		'multidomain' => TRUE,
	);

	return $variables;
}

/**
 * Implements hook_page_alter
 *
 * Adds the GA opt-out JavaScript to the page (must be included _after_ the GA initialization script)
 */
function googleanalytics_opt_out_page_alter(&$page)
{
	$path = drupal_get_path('module', 'googleanalytics_opt_out');

	drupal_add_js(array('googleanalytics_opt_out' => array('success_message' => variable_get('googleanalytics_opt_out_message'))), 'setting');
	// GA script is included in group "JS_DEFAULT"
	drupal_add_js($path . '/googleanalytics_opt_out.js', array('type' => 'file', 'group' => JS_DEFAULT + 1));
}

/**
 * Implements hook_form_FORM_ID_alter() to add a new fieldset to the Google Analytics configuration
 */
function googleanalytics_opt_out_form_googleanalytics_admin_settings_form_alter(&$form, &$form_state, $form_id)
{
	$form['#validate'][] = 'googleanalytics_opt_out_googleanalytics_admin_settings_form_validate';

	$form['opt_out'] = array(
		'#type' => 'fieldset',
		'#title' => t('Opt-out settings'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);

	$form['opt_out']['googleanalytics_opt_out_message'] = array(
		'#title' => t('Success Message'),
		'#type' => 'textarea',
		'#default_value' => variable_get('googleanalytics_opt_out_message', 'Opt-out Successful.'),
		'#cols' => 60,
		'#description' => t('The alert (browser-) message that appears after an opt-out link has been clicked. Leave this empty to show no notification'),
	);

}

function googleanalytics_opt_out_googleanalytics_admin_settings_form_validate($form, &$form_state)
{
	$form_state['values']['googleanalytics_opt_out_message'] = trim($form_state['values']['googleanalytics_opt_out_message']);
}
